# Learning Webpack

These are my study notes and snippets for learning and exploring webpack.

---

## [Webpack 2 - A full tutorial](https://youtu.be/eWmkBNBTbMM)

- My modified tutorial repo w/ less-loader and other adjustments: [webpack-tutorial](webpack-tutorial/)

- Resources
    - Main Repo for Tutorial: https://github.com/emiloberg/webpack-tutorial
    - http://webpack.github.io
    - Video: [Webpack 2 vs Browserify/Grunt/Gulp/Rollup](https://youtu.be/C_ZtQClrVYw)
      - http: //stateofjs.com
      - Code Splitting
      - Dead Code Elimination
      - Hot Module Replacement (HMR)
      - Comparison of major bundlers and task runners (grunt, gulp browserify, rollup, etc)
    - [Difference Webpack and Webpack 2](https://gist.github.com/sokra/27b24881210b56bbaff7)
    - [Code Splitting and React](https://medium.com/modus-create-front-end-development/automatic-code-splitting-for-react-router-w-es6-imports-a0abdaa491e9)
    - [Full tutorial on HMR ](http://andrewhfarmer.com/webpack-hmr-tutorial/)
    - [More about MHR](https://medium.com/@rajaraodv/webpack-hot-module-replacement-hmr-e756a726a07#.m17ffe4km)
    - [Webpack Video Course (pay)](https://egghead.io/courses/using-webpack-for-production-javascript-applications)

---

## [Webpack — The Confusing Parts](https://medium.com/@rajaraodv/webpack-the-confusing-parts-58712f8fcad9) - _(@rajaraodv)_
- [Entry: String, Array, Object](https://medium.com/@rajaraodv/webpack-the-confusing-parts-58712f8fcad9#.x02qhkfre)
    - _String_ - single entry point
    - _Array_ - Append non-dependencies to bundle
    - _Object_ - Output multiple named bundles, e.g. `output.filename: [name].js`
    - _Combos_

```javascript
entry: {
    vendor: ['jquery', 'analytics.js', 'optimizely.js'],
    index: './public/src/index.js',
    profile: './public/src/profile.js'
}
```
- [Chaining Loaders](https://medium.com/@rajaraodv/webpack-the-confusing-parts-58712f8fcad9#.x02qhkfre)
    - Processed right to left: `loader: ‘style!css’ <--(short for style-loader!css-loader)`
    -  Plugins work at bundle or chunk level and usually work at the end of the bundle generation process.

---

[Two Quick Ways To Reduce React App’s Size In Production (w/ Webpack plugins)](https://medium.com/@rajaraodv/two-quick-ways-to-reduce-react-apps-size-in-production-82226605771a)