const path                  = require('path');
const webpack               = require('webpack');
const HTMLWebpackPlugin     = require('html-webpack-plugin');
const ExtractTextPlugin     = require('extract-text-webpack-plugin');
const ExT                   = use => ExtractTextPlugin.extract({use})
const DEVELOPMENT           = process.env.NODE_ENV === 'development';
const PRODUCTION            = process.env.NODE_ENV === 'production';

const entry = PRODUCTION
  ? [
    './src/index.js'
  ]
  : [
    './src/index.js',
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:8080'
  ];

const plugins = PRODUCTION
  ? [
    new webpack.optimize.UglifyJsPlugin(),
    new ExtractTextPlugin('style-[contenthash:10].css'),
    new HTMLWebpackPlugin({
      template: 'index-template.html'
    })
  ]
  : [
    new webpack.HotModuleReplacementPlugin()
  ];

// These are being added so that we can access them in the modules; see index.js
plugins.push(
    new webpack.DefinePlugin({
    DEVELOPMENT: JSON.stringify(DEVELOPMENT),
    PRODUCTION: JSON.stringify(PRODUCTION)
  })
);

const cssIdentifier = PRODUCTION ? '[hash:base64:10]' : '[path][name]---[local]';

const cssLoader = PRODUCTION
  ? ExT([{
      loader: 'css-loader?minimize&localIdentName=' + cssIdentifier // translates CSS into CommonJS
    }, {
      loader: 'less-loader' // compiles Less to CSS
    }])
  : [{ // DEVELOPMENT
      loader: 'style-loader'
    }, {
      loader: 'css-loader?localIdentName=' + cssIdentifier
    }, {
      loader: 'less-loader'
    }]

module.exports = {
  devtool: 'source-map',
  entry: entry,
  plugins: plugins,
  externals: {
  jquery: 'jQuery' //jquery is external and available at the global variable jQuery
  },
  module: {
    rules: [{
      test: /\.less$/,
      use: cssLoader,
      exclude: /node_modules/
    }, {
      test: /\.js$/,
      use: 'babel-loader',
      exclude: /node_modules/
    }, {
      test: /\.(png|jpg|gif)$/,
      use: 'url-loader?limit=10000&name=images/[hash:12].[ext]',
      exclude: /node_modules/
    }]
  },
    output: {
    path: path.join(__dirname, 'dist'),
    publicPath: PRODUCTION ? '/' : '/dist/',
    filename: PRODUCTION ? 'bundle.[hash:12].min.js' : 'bundle.js'
  }
};
